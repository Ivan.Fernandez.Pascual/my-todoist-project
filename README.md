
##TODO LIST AVANZADA##

El principal objetivo de este proyecto es imitar, de forma simplificada, la aplicación todoist constará de as siguientes funcionalidades:

1.- podremos hacer un seguimiento muy intuitivo de nuestras tarjetas.

2.- Podremos añadir tarjetas de forma general pulsando el botón Añadir tarjeta (+) a través de un pequeño formulario.
(Clickando el botón + de la Navbar o el botón Añadir tarea de la lista podremos abrir el formulario de nuevas cartas y añadiremos la tarea al array cards)

3.- Cuando creemos varias tareas las veremos contenidas en una lista en el menú principal.

4.- Tendremos un input de búsqueda que filtrará las cartas que vemos en la pantalla del menu principal.

5.- El icono de la casa nos enviará a la lista del menu principal.

6.- Habrá un nuevo botón que nos permita cambiar de ruta a la lista de tareas completadas (check)

Además se proponen funcionalidades avanzadas que ya hemos añadido a nuestra App :

7.- Hay un boton (Heart) enla navegacion principal, con el  que mostraremos la lista de las tareas favoritas que podremos completar.

8.- Cada Tarea tiene su descripcion, para apuntar detalles o tips que sean utiles para la reaización de la misma.Además esta descripcion es editable por si necesitamos añadir o quitar detalles de la tarea.


9.- La lista de tareas principal se guarda en sesionStorage por lo que persiste aunque cerremos el navegador.

10.- (WIP) Login en la aplicacion. //Mover la lista de `todos` inicial que teníamos en `/` al endpoint `/todos` o similar. Ahora añade un formulario de inicio de sesión en `/` y protege el resto de las rutas para que únicamente puedan acceder usuarios que se han autenticado mediante el formulario y hemos encontrado dicho usuario en el array de la base de datos.//