import React, { Component } from "react";
import { v4 as uuid } from "uuid";
import "../styles/Todoform.scss";

import { Link } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";

export default class Todoform extends Component {
  state = {
    doTask: "",
    doTaskDescription:""
  };

  handleChange = (ev) => {
    const value = ev.target.value;
    this.setState({ doTask: value,
     });

  };

  handleChangeDesc = (ev) => {
    const valueDesc = ev.target.value;
    this.setState({ doTaskDescription: valueDesc,
     });

  };

  handleSubmit = (ev) => {
    ev.preventDefault();
    alert("Tu Tarea ha sido añadida");

    this.props.addTask({
      doTask: this.state.doTask,
      doTaskDescription: this.state.doTaskDescription,
      complete: false,
      favorite:false,
      edit:false,
      edited:'',
      id: uuid(),
    });
    this.setState({
      doTask: "",
      doTaskDescription:"",
    });
  };
  render() {
    return (
      <div className="Form">
        <h3 className="Form__title center">Añade tu Tarea</h3>
        <p className="Form__info"  > Aquí podras añadir cuantas tareas necesites. Éstas aparecerán en el menu de inicio para que puedas administrarlas</p>
        <form className="Form__block center flex" onSubmit={this.handleSubmit}>
          <input
            className="Form__block--title"
            name="doTask"
            placeholder="Task to do"
            value={this.state.doTask}
            onChange={this.handleChange}
          ></input>
           <textarea
           className="Form__block--description"
           rows="3"
            name="doTaskDescription"
            placeholder="Describe your Task"
            value={this.state.doTaskDescription}
            onChange={this.handleChangeDesc}
          ></textarea>
          <button className="Form__block--button"> Añadir Tarea</button>
        </form>
        <div className="center">
          <Link to="/" className="Form__home">
            <FontAwesomeIcon icon={faHome} className="Form__home--icon" /> Back to Home
          </Link>
        </div>
        
      </div>
      
    );
  }
}


  
