import React, { Component } from "react";

import Card from "./Card";
import "../styles/Todolist.scss";
export default class Todolist extends Component {
  render() {
    return (
      <div className="Todolist">
        <h3 className="Todolist__title center">Bandeja de entrada</h3>

        <div className="Todolist__list">
          <div className="Todolist__list--counter center">
            Tareas pendientes:{" "}
            {this.props.todos.filter((todo) => !todo.complete).length}
          </div>
          {this.props.todos.map((todo) => (
            todo.complete === false ?  <Card
              key={todo.id}
              toggleComplete={() => this.props.toggleComplete(todo.id)}
              handleFavorite={() => this.props.handleFavorite(todo.id)}
              handleEdit={() => this.props.handleEdit(todo.id)}
              delete={() => this.props.delete(todo.id)}
              todo={todo}
            ></Card>: null
           
          ))}
        </div>
      </div>
    );
  }
}
