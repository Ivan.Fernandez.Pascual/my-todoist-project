import React, { Component } from "react";
import { Link } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { faCheck } from "@fortawesome/free-solid-svg-icons";


import "../styles/navbar.scss";

export default class navbar extends Component {


  render() {
    return (
      <nav className="Navbar">
        <div className="Navbar__home">
          <Link to="/" className="Navbar__home">
            <FontAwesomeIcon icon={faHome} />
          </Link>
        </div>

        <div className="Navbar__search">
          <span className="Navbar__search-lupe">
            <FontAwesomeIcon icon={faSearch}/>
          </span>
          <input onChange={this.props.handleChange}
            value={this.props.filter}
            type="text"
            placeholder="Busqueda"
            className="Navbar__search-text"
          ></input>
        </div>
        <div className="Navbar__plus">
          <Link to="/TodoForm" className="Navbar__plus">
            <FontAwesomeIcon icon={faPlus} />
          </Link>
        </div>
        <div className="Navbar__favorite">
          <Link to="/favorite-task" className="Navbar__favorite">
            <FontAwesomeIcon icon={faHeart} />
          </Link>
        </div>
        <div className="Navbar__completed">
          <Link to="/completed-task" className="Navbar__completed">
          <FontAwesomeIcon icon={faCheck} />
          </Link>
        </div>

      </nav>
    );
  }
}
