import React, { Component } from "react";
import Card from "./Card";
import '../styles/CompletedTask.scss'

export default class FavoritedTask extends Component {
  render() {
    return (
      <div className='back'>
      <h3 className='center title'>Tareas Favoritas</h3>
        {this.props.favorite.map((todo) => {
          if (todo.favorite === true && todo.complete === false)
            {return (<Card key={todo.id} todo={todo}></Card>)} return null;
        })}
      </div>
    );
  }
}
