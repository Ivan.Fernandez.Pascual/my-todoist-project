import React, { Component } from "react";
import Card from "./Card";
import '../styles/CompletedTask.scss'


export default class CompletedTask extends Component {
  render() {
    return (
      <div>
      <h3 className='center title'>Tareas Completadas
      </h3>
      <div>
        {this.props.completed.map((todo) =>{
          if (todo.complete === true)
            {return <Card key={todo.id} todo={todo}></Card>}return null;
        })}
      </div>
      </div>
    );
  }
}
