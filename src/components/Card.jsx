import React, { Component } from "react";
import "../styles/Card.scss";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { faTrash } from "@fortawesome/free-solid-svg-icons";

export default class Card extends Component {
  state = {
    edited: "",
  };
  toggleEdit = (ev) => {
    const value = ev.target.value;

    this.setState({ edited: value, doTaskDescription: value });
  };

  render() {
    if (this.props.todo.complete === false) {
      if (
        this.props.todo.favorite === false &&
        this.props.todo.edit === false
      ) {
        return (
          <div className="Card">
            <div className="Card__favorite">
              <FontAwesomeIcon
                icon={faHeart}
                className="Card__favorite--icon"
                onClick={this.props.handleFavorite}
              />
              <FontAwesomeIcon
                icon={faPencilAlt}
                className="Card__favorite--icon"
                onClick={this.props.handleEdit}
              />
              <FontAwesomeIcon
                icon={faTrash}
                className="Card__favorite--icon"
                onClick={this.props.delete}
              />
            </div>
            <div className="Card__title" onClick={this.props.toggleComplete}>
              <h4>Tarea sin hacer</h4>
            </div>

            <div className="Card__task">{this.props.todo.doTask}</div>
            <div value={this.state.edited} className="Card__description">
              {this.state.edited
                ? this.state.edited
                : this.props.todo.doTaskDescription}
            </div>
          </div>
        );
      } else if (
        this.props.todo.favorite === true &&
        this.props.todo.edit === false
      ) {
        return (
          <div className="Card">
            <div className="Card__favorite">
              <FontAwesomeIcon
                icon={faHeart}
                className="Card__favorite--icon2"
                onClick={this.props.handleFavorite}
              />
              <FontAwesomeIcon
                icon={faPencilAlt}
                className="Card__favorite--icon"
                onClick={this.props.handleEdit}
              />
              <FontAwesomeIcon
                icon={faTrash}
                className="Card__favorite--icon"
                onClick={this.props.delete}
              />
            </div>
            <div className="Card__title" onClick={this.props.toggleComplete}>
              <h4>Tarea sin hacer</h4>
            </div>

            <div className="Card__task">{this.props.todo.doTask}</div>
            <div value={this.state.edited} className="Card__description">
              {this.state.edited
                ? this.state.edited
                : this.props.todo.doTaskDescription}
            </div>
          </div>
        );
      } else if (
        this.props.todo.favorite === false &&
        this.props.todo.edit === true
      ) {
        return (
          <div className="Card">
            <div className="Card__favorite">
              <FontAwesomeIcon
                icon={faHeart}
                className="Card__favorite--icon"
                onClick={this.props.handleFavorite}
              />
              <FontAwesomeIcon
                icon={faPencilAlt}
                className="Card__favorite--icon2"
                onClick={this.props.handleEdit}
              />
              <FontAwesomeIcon
                icon={faTrash}
                className="Card__favorite--icon"
                onClick={this.props.delete}
              />
            </div>
            <div className="Card__title" onClick={this.props.toggleComplete}>
              <h4>Tarea sin hacer</h4>
            </div>

            <div className="Card__task">{this.props.todo.doTask}</div>
            <div value={this.state.edited} className="Card__description">
              {this.state.edited
                ? this.state.edited
                : this.props.todo.doTaskDescription}
            </div>
            <textarea value={this.state.edited} onChange={this.toggleEdit}>
              edit
            </textarea>
          </div>
        );
      } else if (
        this.props.todo.favorite === true &&
        this.props.todo.edit === true
      ) {
        return (
          <div className="Card">
            <div className="Card__favorite">
              <FontAwesomeIcon
                icon={faHeart}
                className="Card__favorite--icon2"
                onClick={this.props.handleFavorite}
              />
              <FontAwesomeIcon
                icon={faPencilAlt}
                className="Card__favorite--icon2"
                onClick={this.props.handleEdit}
              />
              <FontAwesomeIcon
                icon={faTrash}
                className="Card__favorite--icon"
                onClick={this.props.delete}
              />
            </div>
            <div className="Card__title" onClick={this.props.toggleComplete}>
              <h4>Tarea sin hacer</h4>
            </div>

            <div className="Card__task">{this.props.todo.doTask}</div>
            <div value={this.state.edited} className="Card__description">
              {this.state.edited
                ? this.state.edited
                : this.props.todo.doTaskDescription}
            </div>
            <textarea value={this.state.edited} onChange={this.toggleEdit}>
              edit
            </textarea>
          </div>
        );
      }
    } else {
      return (
        <div className="Card__CardDone" onClick={this.props.toggleComplete}>
          <h4 className="Card__title">Tarea Hecha </h4>
          <div>{this.props.todo.doTask}</div>
          <div className="Card__description">
            {this.props.todo.doTaskDescription}
          </div>
        </div>
      );
    }
  }
}
