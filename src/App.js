import React from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Navbar from "./components/navbar";
import CompletedTask from "./components/CompletedTask";
import FavoritedTask from "./components/FavoritedTask";
import Form from "./components/Todoform";
import Todolist from "./components/Todolist";

class App extends React.Component {
  state = {
    todos: [],
    completed: [],
    favorite: [],
    filter: "",
  };

  componentDidMount() {
    const sessionTodos = sessionStorage.getItem("todos");

    if (!sessionTodos) {
      sessionStorage.setItem("todos", JSON.stringify([]));
      this.setState({
        todos: [],
      });
    } else {
      this.setState({
        todos: JSON.parse(sessionTodos),
      });
    }
  }

  addTask = (task) => {
    console.log(task);
    this.setState(
      {
        todos: [task, ...this.state.todos],
      },
      () => {
        sessionStorage.setItem("todos", JSON.stringify(this.state.todos));
      }
    );
  };

  delete = (id) => {
    this.setState(
      {
        todos: this.state.todos.filter((todo) => {
          if (todo.id !== id) {
            return todo;
          }return null
        }),
      },
      () => {
        sessionStorage.setItem("todos", JSON.stringify(this.state.todos));
      }
    );
  };

  handleChange = (event) => {
    const value = event.target.value;
    this.setState({ filter: value });
  };

  handleEdit = (id) => {
    this.setState(
      {
        todos: this.state.todos.map((todo) => {
          if (todo.id === id) {
            return {
              ...todo,
              edit: !todo.edit,
            };
          } else {
            return todo;
          }
        }),
      },
      () => {
        sessionStorage.setItem("todos", JSON.stringify(this.state.todos));
      }
    );
  };
  
  handleFavorite = (id) => {
    this.setState(
      {
        todos: this.state.todos.map((todo) => {
          if (todo.id === id) {
            return {
              ...todo,
              favorite: !todo.favorite,
            };
          } else {
            return todo;
          }
        }),
      },
      () => {
        sessionStorage.setItem("todos", JSON.stringify(this.state.todos));
      }
    );
  };

  toggleComplete = (id) => {
    this.setState(
      {
        todos: this.state.todos.map((todo) => {
          if (todo.id === id) {
            return {
              ...todo,
              complete: !todo.complete,
            };
          } else {
            return todo;
          }
        }),
      },
      () => {
        sessionStorage.setItem("todos", JSON.stringify(this.state.todos));
      }
    );
  };

  render() {
    const filteredCard = this.state.todos.filter((todo) => {
      const title = todo.doTask.toLowerCase().trim();
      const filter = this.state.filter.toLowerCase().trim();

      return title.includes(filter);
    });
    return (
      <div className="App">
        <Router>
          <Navbar handleChange={this.handleChange} filter={this.state.filter} />
          <Switch>
            <Route
              path="/TodoForm"
              exact
              render={() => (
                <Form addTask={this.addTask} filter={this.state.filter} />
              )}
            />
            <Route
              path="/completed-task"
              exact
              render={() => <CompletedTask completed={this.state.todos}  delete={this.delete} />}
            />
            <Route
              path="/favorite-task"
              exact
              render={() => <FavoritedTask favorite={this.state.todos} />}
            />
            <Route
              path="/"
              render={() => (
                <Todolist
                  delete={this.delete}
                  handleEdit={this.handleEdit}
                  handleFavorite={this.handleFavorite}
                  addTask={this.addTask}
                  toggleComplete={this.toggleComplete}
                  todos={filteredCard}
                />
              )}
            />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
